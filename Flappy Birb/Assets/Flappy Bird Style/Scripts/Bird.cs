﻿using UnityEngine;
using System.Collections;

public class Bird : MonoBehaviour 
{
	public float upForce;					//Upward force of the "flap".
	private bool isDead = false;			//Has the player collided with a wall?

	private Animator anim;					//Reference to the Animator component.
	private Rigidbody2D rb2d;				//Holds a reference to the Rigidbody2D component of the bird.

	public int birbsLife = 5;
	void Start()
	{
		//Get reference to the Animator component attached to this GameObject.
		anim = GetComponent<Animator> ();
		//Get and store a reference to the Rigidbody2D attached to this GameObject.
		rb2d = GetComponent<Rigidbody2D>();
	}

	void Update()
	{
		//Don't allow control if the bird has died.
		if (isDead == false) 
		{
            
			//Look for input to trigger a "flap".
			//if (Input.GetMouseButtonDown(0)) 
			if(Input.GetKeyDown(KeyCode.Space)) //zmiana sterowania, tylko spacja i strzałka w górę
			{
				//...tell the animator about it and then...
				anim.SetTrigger("Flap");
				//...zero out the birds current y velocity before...
				rb2d.velocity = Vector2.zero;
				//	new Vector2(rb2d.velocity.x, 0);
				//..giving the bird some upward force.
				rb2d.AddForce(new Vector2(0, upForce));
			}
			if(Input.GetKeyDown(KeyCode.RightArrow)){
				anim.SetTrigger("Flap");
                rb2d.AddForce(new Vector2(upForce, rb2d.velocity.y));
			}
		}
	}

	void OnCollisionEnter2D(Collision2D other)
	{
		birbsLife--; //umieranie ptaka
		if(birbsLife==0) {
		// Zero out the bird's velocity
		rb2d.velocity = Vector2.zero;
		// If the bird collides with something set it to dead...
		isDead = true;
		//...tell the Animator about it...
		anim.SetTrigger ("Die");
		//...and tell the game control about it.
		GameControl.instance.BirdDied ();}

		if (isDead==true) rb2d.velocity=new Vector2(21,37);
	}

}
