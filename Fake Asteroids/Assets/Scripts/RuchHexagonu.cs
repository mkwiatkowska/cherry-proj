﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RuchHexagonu : MonoBehaviour {

    private Rigidbody2D rb2d;
    private float wartoscPredkosci = 20.0f;

	// Use this for initialization
	void Start () {
        rb2d = GetComponent<Rigidbody2D>();
        rb2d.constraints = RigidbodyConstraints2D.FreezePositionY | RigidbodyConstraints2D.FreezeRotation; //blokada ruchu na osi Y i obrotu
        
    }
	
	// Update is called once per frame
	void Update () {
      
        if (Input.GetKey(KeyCode.A))
        {
            rb2d.AddForce(new Vector2(-wartoscPredkosci, 0.0f));
        }
        if (Input.GetKey(KeyCode.D))
        {
            rb2d.AddForce(new Vector2(wartoscPredkosci, 0.0f));
        }
    }
}
