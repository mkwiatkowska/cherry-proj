﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collision : MonoBehaviour {
    private Rigidbody2D rb2d;
    private int points=0;
    
	// Use this for initialization
	void Start () {
        rb2d = GetComponent<Rigidbody2D>();
        
	}
    private void OnCollisionEnter2D(Collision2D collision)
    {
        
        points++;
        Destroy(gameObject);
        Debug.Log(points);
    }
    // Update is called once per frame
    void Update () {
       
	}
}
