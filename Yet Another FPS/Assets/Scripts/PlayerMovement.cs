﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float speed = 4.0f;
    public float gravity = -9.8f;

    private CharacterController _charCont;

    public float jumpSpeed = 20.0F;
    private Vector3 movement = Vector3.zero;

    // Use this for initialization
    void Start()
    {
        _charCont = GetComponent<CharacterController>();
    }

    void Jump()
    {
        if (_charCont.isGrounded && Input.GetButton("Jump"))
        {
            movement.y = jumpSpeed;
        }
        if (movement.y > 0)
        {
            movement.y += gravity * Time.deltaTime;
        }
        _charCont.Move(movement * Time.deltaTime);
    }

    void Move()
    {
        float deltaX = Input.GetAxis("Horizontal") * speed;
        float deltaZ = Input.GetAxis("Vertical") * speed;
        Vector3 movement = new Vector3(deltaX, 0, deltaZ);
        movement = Vector3.ClampMagnitude(movement, speed); //Limits the max speed of the player

        movement.y = gravity;

        movement *= Time.deltaTime;     //Ensures the speed the player moves does not change based on frame rate
        movement = transform.TransformDirection(movement);
        _charCont.Move(movement);
    }

    // Update is called once per frame
    void Update()
    {
        Move();
        Jump();
    }
}